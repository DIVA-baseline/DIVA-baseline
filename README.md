# DockerFile for RC3D

The folder contains the docker file to build RC3D with DIVA

## Dependencies
1. [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Nvidia Docker 2](https://github.com/NVIDIA/nvidia-docker)

## Build
    sudo nvidia-docker build -t rc3d-stable .

## Run The Container
    sudo nvidia-docker run -ti --name rc3d \
                        --mount type=bind,source=/data/diva,target=/data/diva \
                        rc3d-stable:latest

### Note: modify the source in --mount to the root directory of VIRAT-V1 data.

## Setup RC3D
    source /opt/kitware/setup_DIVA.sh    

### Note: Further instructions related to virat based experiments are available in experiments/virat folder in RC3D root
