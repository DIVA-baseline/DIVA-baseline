FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
                    build-essential \
                    libgl1-mesa-dev \
                    libexpat1-dev \
                    libgtk2.0-dev \ 
                    liblapack-dev \
                    python2.7-dev \
                    git \
                    cmake \
                    vim \
                    wget \
                    cmake-curses-gui \
                    python-dev \
                    python-numpy \
                    python-pip \
                    python-setuptools \
                    python-scipy 

ENV DIVA_INSTALL=/opt/kitware/DIVA
WORKDIR /diva

RUN ln -sf /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/libcuda.so && \
    mkdir /diva/src && \
    git clone https://github.com/Kitware/DIVA.git /diva/src && \
    cd /diva/src && git checkout tags/Json_File_Index_Based_Testing && cd .. && \
    mkdir -p build/release &&  \
    cd build/release && \   
    cmake ../../src -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$DIVA_INSTALL \
                    -DDIVA_BUILD_WITH_CUDA=ON -DDIVA_BUILD_WITH_CUDNN=ON \
                    -DDIVA_SUPERBUILD=ON && \
    make -j8

ENV RC3D_ROOT=/diva/R-C3D


#RUN /bin/bash -c "source /opt/kitware/DIVA/install/setup_DIVA.sh" && \
RUN git clone https://gitlab.kitware.com/kwiver/R-C3D.git $RC3D_ROOT && \
    cd $RC3D_ROOT && \
    git checkout tags/Json_File_Index_Based_Testing -b docker && \
    cd caffe3d && pip install --upgrade pip==9.0.3 && \
    cd python && for req in $(cat requirements.txt) pydot easydict jsonschema munkres; do pip install $req; done && cd .. && \
    mkdir build && cd build && \
    cmake -DCMAKE_PREFIX_PATH:PATH=$DIVA_INSTALL \
          -DBLAS=Open \
          -DCUDA_ARCH_NAME=Manual \
          -DCUDA_ARCH_BIN="61" -DCUDA_ARCH_PTX="61" \
          .. && \
    make -j4 && \
    cd $RC3D_ROOT/lib && make &&\
    mkdir -p $RC3D_ROOT/experiments/virat/pretrain && \
    wget -O $RC3D_ROOT/experiments/virat/pretrain/ucf101.caffemodel \
        https://data.kitware.com/api/v1/file/5b47c5178d777f2e6225a757/download 






